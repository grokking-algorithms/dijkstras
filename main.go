package main

import (
	"fmt"
	"math"
)

var processed []string

func main() {
	graph := map[string]map[string]float64{}
	graph["start"] = map[string]float64{}
	graph["a"] = map[string]float64{}
	graph["b"] = map[string]float64{}
	graph["fin"] = map[string]float64{}
	graph["start"]["b"] = 2
	graph["a"]["fin"] = 1
	graph["b"]["a"] = 3
	graph["b"]["fin"] = 5
	graph["fin"] = nil

	costs := make(map[string]float64)
	costs["a"] = 6
	costs["b"] = 2
	costs["fin"] = math.Inf(0)

	parents := make(map[string]string)
	parents["a"] = "start"
	parents["b"] = "start"
	parents["in"] = ""

	node := findLowestCostNode(costs)

	for node != "" {
		cost := costs[node]
		neighbors := graph[node]

		for n, v := range neighbors {
			newCost := cost + v
			if costs[n] > newCost {
				costs[n] = newCost
				parents[n] = node
			}
		}
		processed = append(processed, node)
		node = findLowestCostNode(costs)
	}

	fmt.Printf("%v", costs)
}

func findLowestCostNode(node map[string]float64) string {
	lowestVal := math.Inf(0)
	lowestKey := ""

	for k, v := range node {
		if v < lowestVal && !nodeInProcessed(k) {
			lowestVal = v
			lowestKey = k
		}
	}
	return lowestKey
}

func nodeInProcessed(node string) bool {
	for _, a := range processed {
		if a == node {
			return true
		}
	}
	return false
}
